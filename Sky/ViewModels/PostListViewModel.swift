//
//  PostListViewModel.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 31/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation
import Combine

class PostListViewModel: ObservableObject {
    
    @Published var searchText = ""
    @Published private (set) var posts = [PostViewModel]()
    @Published var noData = false
    
    private var searchCancellable: Cancellable? {
        didSet {
            oldValue?.cancel()
        }
    }
    
    deinit {
        searchCancellable?.cancel()
    }
    
    init() {
        searchCancellable = $searchText
            .debounce(for: 0.5, scheduler: DispatchQueue.main)
            .removeDuplicates()
            .filter { !$0.isEmpty }
            .flatMap { (keyword) -> AnyPublisher<[Post], Never> in
                print("keyword: \(keyword)")
                return RedditAPI.getImagesBy(keyword: keyword)
                    .eraseToAnyPublisher()
            }
            .map {
                return $0.map(PostViewModel.init)
            }
            .replaceError(with: [])
            .sink(receiveValue: { posts in
                self.posts = posts
                self.noData = posts.isEmpty
            })
    }
}
