//
//  PostViewModel.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 31/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation

struct PostViewModel: Identifiable {
    
    let id: String
    let title: String
    let author: String
    let date: String
    let thumbnail: URL
    let url: URL
    let comments: String
}

extension PostViewModel {
    
    init(post: Post) {
        id = post.id
        title = post.title
        author = post.author
        thumbnail = URL(string: post.thumbnail)!
        url = URL(string: post.url)!
        let createdDate = Date(timeIntervalSince1970: post.created_utc)
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .medium
        date = dateFormatter.string(from: createdDate)
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .decimal
        comments = formatter.string(from: NSNumber(value: post.num_comments))!
    }
}
