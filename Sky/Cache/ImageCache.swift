//
//  ImageCache.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 31/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import UIKit

// Abstraction Layer on top of NSCache used to cache Reddit images
protocol ImageCache {
    subscript(_ url: URL) -> UIImage? { get set }
}

struct DefaultImageCache: ImageCache {
    private let cache = NSCache<NSURL, UIImage>()
    
    subscript(_ key: URL) -> UIImage? {
        get {
            cache.object(forKey: key as NSURL)
        }
        set {
            newValue == nil
                ? cache.removeObject(forKey: key as NSURL)
                : cache.setObject(newValue!, forKey: key as NSURL)
        }
    }
}
