//
//  DataCache.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 01/06/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation

public class DataCache {
    
    enum Directory {
        case cache
        case documents
    }
    
    /// Get the URL of the specified directory
    ///
    /// - Parameters:
    ///   - directory: Directory (cache / document)
    /// - Returns: URL of the specified directory
    static private func getURL(for directory: Directory) -> URL {
        var path: FileManager.SearchPathDirectory
        switch directory {
        case .cache: path = .cachesDirectory
        case .documents: path = .documentDirectory
        }
        return FileManager.default.urls(for: path, in: .userDomainMask).first!
    }
    
    /// Get an encodable object from the specified directory if it exists
    ///
    /// - Parameters:
    ///   - fileName: The file name where the object is stored
    ///   - directory: Directory (cache / document)
    /// - Returns: Decodable object if it exists
    static func get<T: Decodable>(_ fileName: String, from directory: Directory, as type: T.Type) -> T? {
        let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
        do {
            if let data = FileManager.default.contents(atPath: url.path) {
                return try JSONDecoder().decode(type, from: data)
            }
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    /// Save an encodable object to the specified directory
    ///
    /// - Parameters:
    ///   - object: Encodable object
    ///   - directory: Directory (cache / document)
    ///   - fileName: The file name saved to the specified directory
    static func set<T: Encodable>(_ object: T, to directory: Directory, as fileName: String) {
        let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
        do {
            let data = try JSONEncoder().encode(object)
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: url.path) {
                try fileManager.removeItem(at: url)
            }
            fileManager.createFile(atPath: url.path, contents: data, attributes: nil)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    /// Clear all the contents of the specified directory
    ///
    /// - Parameters:
    ///   - directory: Directory (cache / document)
    static func clear(_ directory: Directory) {
        do {
            let url = getURL(for: directory)
            let contents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [])
            for fileURL in contents {
                try FileManager.default.removeItem(at: fileURL)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
