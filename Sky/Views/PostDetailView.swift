//
//  PostDetailView.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 31/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation
import SwiftUI

struct PostDetailView: View {
    
    @Environment(\.imageCache) var cache: ImageCache
    
    private let viewModel: PostViewModel
    
    init(viewModel: PostViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ScrollView {
            VStack {
                self.postImageView
                self.titleView
                self.authorView
                self.commentsView
            }
        }
    }
}

extension PostDetailView {
    
    private var postImageView: some View {
        PostImageView(url: viewModel.url, cache: self.cache)
            .animation(.easeInOut)
    }
    
    private var titleView: some View {
        VStack {
            Text(viewModel.title)
                .font(.headline)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
                .padding()
            Text(viewModel.date)
        }
    }
    
    private var authorView: some View {
        HStack {
            Image(systemName: "person.fill")
            Text(viewModel.author)
                .fontWeight(.light)
        }
        .font(.title)
    }
    
    private var commentsView: some View {
        HStack {
            Image(systemName: "message")
            Text(viewModel.comments)
                .fontWeight(.light)
        }
        .font(.title)
    }
}
