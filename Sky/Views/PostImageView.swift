//
//  PostImageView.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 01/06/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import SwiftUI

struct PostImageView: View {
    
    @ObservedObject private var downloader: ImageDownloader

    init(url: URL, cache: ImageCache? = nil) {
        downloader = ImageDownloader(url: url, cache: cache)
    }
    
    var body: some View {
        image
            .onAppear(perform: downloader.load)
            .onDisappear(perform: downloader.cancel)
    }
    
    private var image: some View {
        Group {
            if downloader.image != nil {
                Image(uiImage: downloader.image!)
                    .resizable()
                    .scaledToFit()
            } else {
                Rectangle().foregroundColor(Color.white)
            }
        }
    }
}
