//
//  Post.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 30/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation

struct Post: Codable, Identifiable {
    let id: String
    let title: String
    let author: String
    let thumbnail: String
    let url: String
    let num_comments: Int
    let post_hint: String
    let created_utc: Double
    
    var isImage: Bool {
        return post_hint == "image"
    }
}
