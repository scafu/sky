//
//  Listing.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 30/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation

struct Listing: Decodable {
    let data: ListingData
    
    struct ListingData: Decodable {
        let children: [PostData]
        
        struct PostData: Decodable {
            let data: Post
        }
    }
}
