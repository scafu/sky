//
//  PostListView.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 30/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import SwiftUI
import Grid

struct PostListView: View {
    
    @ObservedObject private var viewModel = PostListViewModel()
    @Environment(\.imageCache) var cache: ImageCache
    
    private var style = StaggeredGridStyle(.vertical,
                                           tracks: .count(3),
                                           spacing: 4)
    
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(text: $viewModel.searchText,
                          placeholder: NSLocalizedString("home_searchBar_placeholder",
                                                         comment: "Search bar placeholder"))
                ScrollView(style.axes) {
                    if viewModel.noData {
                        Text(NSLocalizedString("home_noresults_found", comment: "no results found."))
                    } else {
                        Grid(viewModel.searchText.isEmpty ? [] : viewModel.posts) { post in
                            NavigationLink(destination: PostDetailView(viewModel: post)) {
                                PostImageView(url: post.thumbnail, cache: self.cache)
                            }
                            .buttonStyle(PlainButtonStyle())
                        }
                    }
                }.navigationBarTitle(Text(NSLocalizedString("home_navigation_title",
                                                           comment: "Navigation title")))
                .animation(.easeInOut)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct PostListView_Previews: PreviewProvider {
    static var previews: some View {
        PostListView()
    }
}
