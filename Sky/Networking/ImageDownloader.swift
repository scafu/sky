//
//  ImageDownloader.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 01/06/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Combine
import UIKit

class ImageDownloader: ObservableObject {
    
    @Published var image: UIImage?
    
    private let url: URL
    private var cache: ImageCache?
    private var cancellable: AnyCancellable?
    private(set) var isLoading = false
    private static let queue = DispatchQueue(label: "sky_imageDownloader")
    
    init(url: URL, cache: ImageCache? = nil) {
        self.url = url
        self.cache = cache
    }
    
    deinit {
        cancellable?.cancel()
    }
    
    func load() {
        guard !isLoading else { return }

        if let image = cache?[url] {
            self.image = image
            return
        }
        
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .handleEvents(receiveSubscription: { [weak self] _ in self?.onStart() },
                          receiveOutput: { [weak self] in self?.cache($0) },
                          receiveCompletion: { [weak self] _ in self?.onFinish() },
                          receiveCancel: { [weak self] in self?.onFinish() })
            .subscribe(on: Self.queue)
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }
    
    func cancel() {
        cancellable?.cancel()
    }
    
    private func onStart() {
        isLoading = true
    }
    
    private func onFinish() {
        isLoading = false
    }
    
    private func cache(_ image: UIImage?) {
        image.map {
            cache?[url] = $0
        }
    }
}
