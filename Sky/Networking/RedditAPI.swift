//
//  RedditAPI.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 30/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation
import Combine

enum RedditAPI {
    
    static let url = URL(string: "https://www.reddit.com")!
    
    static func getImagesBy(keyword: String) -> AnyPublisher<[Post], Never> {
        let request = URLRequest(url: url.appendingPathComponent("r/\(keyword)/top.json"))
        return URLSession.shared.call(request: request)
            .decode(type: Listing.self, decoder: JSONDecoder())
            .map {
                let posts = $0.data.children
                    .compactMap({ $0.data })
                    .filter({ $0.isImage })
                // Set to DataCache
                DataCache.set(posts, to: .cache, as: keyword)
                return posts
            }
            .replaceError(with: DataCache.get(keyword, from: .cache, as: [Post].self) ?? [])
            .eraseToAnyPublisher()
    }
}
