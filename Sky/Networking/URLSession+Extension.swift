//
//  URLSession+Extension.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 30/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation
import Combine

extension URLSession {
    
    func call(request: URLRequest) -> AnyPublisher<Data, Error> {
        return URLSession.shared.dataTaskPublisher(for: request)
            .receive(on: DispatchQueue.main)
            .tryMap { data, response -> Data in
                let httpResponse = response as? HTTPURLResponse
                if let httpResponse = httpResponse, 200..<300 ~= httpResponse.statusCode {
                    return data
                }
                throw URLError(.badServerResponse)
            }
            .eraseToAnyPublisher()
    }
}
