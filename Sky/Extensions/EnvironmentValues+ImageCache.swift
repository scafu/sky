//
//  EnvironmentValues+ImageCache.swift
//  Sky
//
//  Created by Vincenzo Scafuto on 31/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import SwiftUI

// This extension is used in order to share ImageCache
// DefaultImageCache created when we access for the first time to @Environment property wrapper
struct ImageCacheKey: EnvironmentKey {
    static let defaultValue: ImageCache = DefaultImageCache()
}

extension EnvironmentValues {
    var imageCache: ImageCache {
        get { self[ImageCacheKey.self] }
        set { self[ImageCacheKey.self] = newValue }
    }
}
