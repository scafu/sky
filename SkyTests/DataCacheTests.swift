//
//  DataCacheTests.swift
//  SkyTests
//
//  Created by Vincenzo Scafuto on 01/06/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import XCTest

class DataCacheTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCache() throws {
        // Test Primitive Data
        let string = "test"
        DataCache.set(string, to: .cache, as: "string")
        let cacheString = DataCache.get("string", from: .cache, as: String.self)
        XCTAssertEqual(string, cacheString)
        // Int array
        let intArray = [1, 2, 3, 4, 5]
        DataCache.set(intArray, to: .cache, as: "intArray")
        let cacheIntArray = DataCache.get("intArray", from: .cache, as: [Int].self)
        XCTAssertEqual(intArray, cacheIntArray)
    }
    
    func testDecodableCache() throws {
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: "bmw", ofType: "json")!
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let listing = try JSONDecoder().decode(Listing.self, from: data)
        let posts = listing.data.children.map({ $0.data })
        DataCache.set(posts, to: .cache, as: "test_cachePosts")
        let cachedPosts = DataCache.get("test_cachePosts", from: .cache, as: [Post].self)
        XCTAssertNotNil(cachedPosts)
        XCTAssertEqual(posts.count, cachedPosts!.count)
        XCTAssertEqual(posts.first?.id, cachedPosts?.first?.id)
    }
}
