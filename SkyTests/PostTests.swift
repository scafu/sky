//
//  PostTests.swift
//  SkyTests
//
//  Created by Vincenzo Scafuto on 31/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import XCTest

class PostTests: XCTestCase {

    var decoder: JSONDecoder!
    
    override func setUpWithError() throws {
        decoder = JSONDecoder()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDecode() throws {
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: "bmw", ofType: "json")!
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let listing = try decoder.decode(Listing.self, from: data)
        // Test decoding and number of elements
        XCTAssertNotNil(listing)
        let posts = listing.data.children.map({ $0.data })
        XCTAssertEqual(posts.count, 25)
        // Filter only image "post_hint"
        let onlyImage = posts.filter({ $0.isImage })
        XCTAssertEqual(onlyImage.count, 22)
        // Check id value (for Identifiable protocol)
        XCTAssertEqual(onlyImage[0].id, "gt5lx3")
        // Check if URL Casting (for thumbnail and url property) succeeded
        onlyImage.forEach ({
            print($0.thumbnail)
            XCTAssertTrue($0.thumbnail.isValidURL)
            XCTAssertTrue($0.url.isValidURL)
        })
        
    }

}
