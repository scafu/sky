//
//  String+Extension.swift
//  SkyTests
//
//  Created by Vincenzo Scafuto on 31/05/2020.
//  Copyright © 2020 Vincenzo Scafuto. All rights reserved.
//

import Foundation

extension String {
    
    // Extension used to Test URLs. It is valid if the match covers the whole string
    var isValidURL: Bool {
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: self)
    }
}
